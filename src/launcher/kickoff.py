from . import common

# Command string for the launcher
def command(length, prompt):
    """Command string for Fuzzel as subprocess list."""
    return ["kickoff", "--from-stdin", "-p " + str(prompt)]

def start(list_string, length, prompt):
    """External: Launches the launcher with the specified input, and
    returns the standard output.

    """
    return common.launch(list_string, length, prompt, command)
