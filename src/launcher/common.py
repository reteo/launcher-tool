import subprocess

# Show the launcher with the appropriate details
def launch(list_string, length, prompt, launcher_command):
    """Launches the launcher, using the specified command, and
    returning the standard output of the launcher.

    """
    return subprocess.run(launcher_command(length, prompt),
                          input = list_string.encode(),
                          capture_output=True).stdout.decode()
