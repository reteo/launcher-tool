import argparse
import textwrap


# When I add newlines to documentation, I *expect* them to be there!
class RawFormatter(argparse.HelpFormatter):
    def _fill_text(self, text, width, indent):
        return "\n".join(
            [
                textwrap.fill(line, width)
                for line in textwrap.indent(textwrap.dedent(text), indent).splitlines()
            ]
        )


def parser(compositor_string: str, launcher_string: str, dialog_string: str):
    """Create an argument parser for the main loop."""
    parser = argparse.ArgumentParser(
        prog="Launcher Tool",
        description="Expands dmenu-compatible launchers with additional features.",
        epilog=(
            "Currently, the following are supported by launchertool:\n\n"
            + "Compositors: "
            + compositor_string
            + "\n"
            + "Launchers: "
            + launcher_string
            + "\n"
            + "Dialogs: "
            + dialog_string
            + "\n\n"
        ),
        formatter_class=RawFormatter,
    )

    parser.add_argument("compositor", help="Select the compositor to use.")
    parser.add_argument("launcher", help="Select your preferred launcher.")
    parser.add_argument("dialog", help="Select the desired dialog box.")

    return parser.parse_args()
