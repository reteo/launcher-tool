"""Support for the hyprland compositor."""

import subprocess
import json

### Collect Information ###

# Collect information from hyprctl

# Internal: Uses hyprland infrastructure to extract window IDs,
# titles, and workspaces.  Additional information can be added when
# needed.


def import_clients():
    """Imports client details as JSON using hyprctl."""
    return json.loads(
        subprocess.run(
            ["hyprctl", "-j", "clients"], capture_output=True
        ).stdout.decode()
    )


### Client List: Use with "window" launcher.

# Internal: Goes through the client list to make sure that any items
# with an empty title, class, and workspace are not included in the
# final list.


def clean_clients(client_list):
    """Looks through the client list to make sure no "empty" clients
    are being used.

    """
    return list(
        filter(
            lambda item: item["title"] != ""
            and item["class"] != ""
            and item["workspace"] != "",
            client_list,
        )
    )


# External: Create a list, with each element consisting of an ID,
# workspace, and title.


def client_list():
    """External Function: Creates a list of windows, each one a list
    of three things, an id number, a workspace name, and a window
    title.

    """
    return [
        [item["address"], item["workspace"]["name"], item["title"]]
        for item in clean_clients(import_clients())
    ]


### Workspaces: Use with "goto" and "move" launchers

# Internal: Create a workspace dictionary, in order to ensure all
# workspaces only occur once.


def make_workspace_dict():
    """Creates a dict of workspaces; meant to remove duplicates."""
    return {item["workspace"]["name"]: None for item in clean_clients(import_clients())}


# External: Create a list, with each element consisting of a workspace name.


def workspace_list():
    """External Function: Makes a list of current workspace names."""
    return [workspace for workspace in filter(lambda x: "special" not in x, make_workspace_dict().keys())]

# External: Create a list, with each element consisting of a special workspace name.


def special_workspace_list():
    """External Function: Makes a list of current workspace names."""
    return [workspace[8:] for workspace in filter(lambda x: "special" in x, make_workspace_dict().keys())]


### Perform Actions ###

# External: Execute


def execute(name):
    """External Function: Use compositor to execute command."""
    if name.strip() != "":
        subprocess.run(["hyprctl", "dispatch", "exec", name])


# External: Focus Window


def window_focus(id_number):
    """External Function: Focus on specified window."""
    if id_number.strip() != "":
        subprocess.run(
            ["hyprctl", "dispatch", "focuswindow", "address:" + str(id_number).strip()]
        )


# External: Toggle Special Layer


def special_workspace_toggle(name):
    """External Function: Change focus to specified workspace."""
    if name.strip() != "":
        workspace = name.strip()
        subprocess.run(["hyprctl", "dispatch", "togglespecialworkspace", workspace])


# External: Go to Workspace


def workspace_focus(name):
    """External Function: Change focus to specified workspace."""
    if name.strip() != "":
        workspace = name.strip() if name.strip().isdigit() else "name:" + name.strip()
        subprocess.run(["hyprctl", "dispatch", "workspace", workspace])


# External: Move Current Window to Desktop


def window_workspace_change_focus(name):
    """External Function: Change current window and focus to specified
    workspace.

    """
    if name.strip() != "":
        workspace = name.strip() if name.strip().isdigit() else "name:" + name.strip()
        subprocess.run(["hyprctl", "dispatch", "movetoworkspace", workspace])


# External: Move Current Window to Desktop without changing desktop


def window_workspace_change_silent(name):
    """External Function: Change current window to specified workspace
    without changing workspace focus.

    """
    if name.strip() != "":
        workspace = name.strip() if name.strip().isdigit() else "name:" + name.strip()
        subprocess.run(["hyprctl", "dispatch", "movetoworkspacesilent", workspace])


# External: Move Current Window to Desktop


def window_special_workspace_change_focus(name):
    """External Function: Change current window and focus to specified
    workspace.

    """
    if name.strip() != "":
        workspace = "special:" + name.strip()
        subprocess.run(["hyprctl", "dispatch", "movetoworkspace", workspace])


# External: Move Current Window to Desktop without changing desktop


def window_special_workspace_change_silent(name):
    """External Function: Change current window to specified workspace
    without changing workspace focus.

    """
    if name.strip() != "":
        workspace = "special:" + name.strip()
        subprocess.run(["hyprctl", "dispatch", "movetoworkspacesilent", workspace])
