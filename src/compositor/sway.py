import subprocess
import json

### Collect Information ###

# Collect information from swaymsg

# Internal: uses sway infrastructure to extract window IDs, titles,
# and workspaces.  Additional information can be added when needed.


def import_tree():
    """Imports tree details as JSON using swaymsg."""
    return json.loads(
        subprocess.run(
            ["swaymsg", "-t", "get_tree"], capture_output=True
        ).stdout.decode()
    )


### Client List ###

# Internal: Create a list, with each element consisting ofa dict with
# an ID, workspace, and title.


def client_dict_list(workspace: str, node_list: list):
    """Collect a list of client programs from the specified subtree."""
    result = []

    # We will be going through the list of nodes, one at a time.
    for item in node_list:
        # Workspaces and outputs with no nodes are ignored.
        if (
            item["type"] in ["workspace", "output"]
            and item["nodes"] == []
            and item["floating_nodes"] == []
        ):
            continue

        # Actual windows and tiles are immediately identified.
        if item["type"] in ["con", "floating_con"] and item["nodes"] == []:
            result.append(
                {"title": item["name"], "id": item["id"], "workspace": workspace}
            )
            continue

        # If the item is a workspace, update the workspace name.
        if item["type"] == "workspace":
            workspace = item["name"]

        # Recursively scan through tiled sub-nodes.
        result = result + (client_dict_list(workspace, item["nodes"]))

        # Recursively scan through floating sub-nodes.
        result = result + (client_dict_list(workspace, item["floating_nodes"]))

    return result


# External: Create a list, with each element consisting of an ID,
# workspace, and title.


def client_list():
    """External Function: Create a list of windows, each one a list of
    three things: an ID number, a workspace name, and a window title."""
    return [
        [item["id"], item["workspace"], item["title"]]
        for item in client_dict_list("root", import_tree()["nodes"])
    ]


### Workspaces: Use with "goto" and "move" launchers

# Internal: Create a workspace set, in order to ensure all
# workspaces only occur once.


def make_workspace_list(node_list: list):
    """Collect a list of workspaces; meant to remove duplicates"""

    result = []

    # We will be going through the list of nodes, one at a time.
    for item in node_list:
        # print(item, "\n\n")

        # Entries outside of workspaces and outputs are ignored
        if item["type"] not in ["workspace", "output"]:
            continue
        # Workspaces are recorded.  No nodes means we simply move on.
        if item["type"] in ["workspace"]:
            result = result + [item["name"]]
            continue
        # Recursively scan workspaces and outputs.
        if item["type"] in ["workspace", "output"]:
            result = result + make_workspace_list(item["nodes"])

    return list(set(result))


# External: Create a list, with each element consisting of a workspace name.


def workspace_list():
    """External Function: Make a list of current workspace names."""
    return [
        workspace
        for workspace in filter(
            lambda x: "__i3_scratch" not in x,
            make_workspace_list(import_tree()["nodes"]),
        )
    ]


# External: Create a list, with each element consisting of a scratch
# pad name.  Mostly useless, as sway only has the one scratch pad.


def special_workspace_list():
    """External Function: Make a list of current workspace names."""
    return [
        workspace
        for workspace in filter(
            lambda x: "__i3_scratch" in x,
            make_workspace_list(import_tree()["nodes"]),
        )
    ]


### Perform Actions ###

# External: Execute


def execute(name):
    """External Function: Use compositor to execute command."""
    if name.strip() != "":
        subprocess.run(["swaymsg", "exec", name])


# External: Focus window


def window_focus(id_number):
    """External Function: Focus on specified window."""
    if id_number.strip() != "":
        subprocess.run(["swaymsg", f"[con_id={id_number}]", "focus"])


# External: Toggle Special Layer

# NOTE: Since sway only has the one scratchpad, and it cycles between
# the different windows in the scratchpad, there's not really a need
# for the name.  It's just here to ensure that the program works as
# expected.


def special_workspace_toggle(name):
    """External Function: Change focus to specified scratchpad."""
    if name.strip() != "":
        subprocess.run(["swaymsg", "scratchpad", "show"])


# External: Go to Workspace


def workspace_focus(name):
    """External Function: Change focus to specified workspace."""
    if name.strip() != "":
        subprocess.run(["swaymsg", "workspace", name])


# External: Move Current Window to Desktop


def window_workspace_change_focus(name):
    """External Function: Change current window and focus to specified
    workspace.

    """
    if name.strip() != "":
        subprocess.run(["swaymsg", "move", "workspace", name])
        subprocess.run(["swaymsg", "workspace", name])


# External: Move Current Window to Desktop without changing desktop


def window_workspace_change_silent(name):
    """External Function: Change current window to specified workspace
    without changing workspace focus.

    """
    if name.strip() != "":
        subprocess.run(["swaymsg", "move", "workspace", name])


# External: Move Current Window to scratchpad


def window_special_workspace_change_focus(name):
    """External Function: Change current window and focus to specified
    workspace.

    """
    if name.strip() != "":
        subprocess.run(["swaymsg", "move", "window", "scratchpad"])
        subprocess.run(["swaymsg", "scratchpad", "show"])


# External: Move Current Window to scratchpad without changing desktop


def window_special_workspace_change_silent(name):
    """External Function: Change current window to specified workspace
    without changing workspace focus.

    """
    if name.strip() != "":
        subprocess.run(["swaymsg", "move", "window", "scratchpad"])
