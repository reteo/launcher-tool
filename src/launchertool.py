from sys import exit

from parser import parser
from options import options, select

from dialog import (
    goto,
    move,
    move_silent,
    run,
    window,
    special,
    special_move,
    special_move_silent,
)
from compositor import hyprland, sway, i3
from launcher import dmenu, rofi, wofi, tofi, fuzzel, bemenu, kickoff

# First, we want to set up lookup tables, to convert strings into
# their module counterparts.

COMPOSITORS = {"hyprland": hyprland, "sway": sway, "i3": i3}

LAUNCHERS = {
    "bemenu": bemenu,
    "dmenu": dmenu,
    "fuzzel": fuzzel,
    "kickoff": kickoff,
    "rofi": rofi,
    "tofi": tofi,
    "wofi": wofi,
}

DIALOGS = {
    "goto": goto,
    "move": move,
    "move_silent": move_silent,
    "run": run,
    "window": window,
    "special": special,
    "special_move": special_move,
    "special_move_silent": special_move_silent,
}


# Now, we assign these modules according to the command line options
# and run them.
def main():
    arguments = parser(options(COMPOSITORS), options(LAUNCHERS), options(DIALOGS))

    compositor = select("compositor", arguments.compositor, COMPOSITORS)
    launcher = select("launcher", arguments.launcher, LAUNCHERS)
    dialog = select("dialog", arguments.dialog, DIALOGS)

    dialog.execute(compositor, launcher)


if __name__ == "__main__":
    main()
