def make_window_string(compositor):
    """Makes the list string used by the launcher."""
    return "\n".join(
        [
            "{:<15}    {:<15} {:<100}".format(item[0], item[1], item[2])
            for item in compositor.client_list() if "special" not in item[1]
        ]
    )


def extract_id(window_string):
    """Extracts the ID code from the selection returned by the launcher."""
    return (
        window_string[:15]
        if (window_string is not None)
        else ""
    )


def execute(compositor, launcher):
    """Use the launcher to get the workspace for the compositor to move the window to."""
    compositor.window_focus(
        extract_id(launcher.start(make_window_string(compositor), 120, "Window: "))
    )
