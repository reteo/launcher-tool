def make_workspace_string(compositor):
    """Makes the list string used by the launcher."""
    return '\n'.join(sorted(compositor.special_workspace_list()))
    
def execute(compositor, launcher):
    """Use the launcher to get the workspace for the compositor to move the window to."""
    compositor.window_special_workspace_change_silent(
        launcher.start(
            make_workspace_string(compositor), 30, "Workspace: "))
