import os
import subprocess
from functools import reduce

def make_path_list():
    """Makes a list out of the PATH environment variable."""
    return  os.environ['PATH'].split(":")

def find_executables(path_list):
    """Goes through each directory in the PATH, looks for all
    executable files and symlinks, and returns a list of the results
    for each."""
    return [subprocess.run(
        ["find", directory, "-executable", "-type", "f,l", "-maxdepth", "1", "-printf", "%f\\n"],
        capture_output=True).stdout.decode().split("\n") for directory in path_list]

def make_executable_list(executables_list):
    """Takes a list of lists, and condenses all of them down to a
    single list of executables.

    """
    return sorted(
        list(
            filter(
                lambda item: item != '', reduce(lambda item, addition: item + addition, executables_list)
            )
        ), key=str.casefold
    )

def make_executable_list_string(executable_list):
    """Takes a list of executables, and converts them into a string
    list.

    """
    return '\n'.join(executable_list)

def execute(compositor, launcher):
    """Use the launcher to get the command to execute."""
    compositor.execute(launcher.start(
        make_executable_list_string(
            make_executable_list(
                find_executables(
                    make_path_list()
                ))), 80, "$ "))
