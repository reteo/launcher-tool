def make_workspace_string(compositor):
    """Makes the list string used by the launcher."""
    return "\n".join(sorted(compositor.special_workspace_list()))


def execute(compositor, launcher):
    """Use the launcher to get the workspace for the compositor to change focus to."""
    compositor.special_workspace_toggle(launcher.start(make_workspace_string(compositor), 30, "Overlay: "))
