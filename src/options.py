"""Parses the options to either return a selection or display the
options as a grammatical list"""

def make_option_list_string(option_list) -> str:
    """Converts a list of strings into a list string that is
    grammatically correct.

    """
    if not option_list:
        return ""
    elif len(option_list) == 1:
        return str(list(option_list)[0])
    else:
        return ", ".join(list(option_list)[:-1]) + ", and " + list(option_list)[-1]


def select(role: str, selection: str, option_dict: dict):
    """Takes a role, the selection string, and the option dictionary,
    and converts the result into the value.  If the argument is not in
    the list, return an error instead.

    """
    try:
        return option_dict[selection]
    except KeyError:
        exit(
            'Unfortunately, "'
            + selection
            + '" is not available as a '
            + role
            + ".  Current options are:\n"
            + make_option_list_string(option_dict.keys())
        )


def options(option_dict):
    """Shows the list of options available from the specified option
    dictionary.

    """
    return make_option_list_string(option_dict.keys())
